export default {
    fetch() {
        const args = Array.prototype.slice.call(arguments);
        const action = args[0][0];
        const parameter = JSON.stringify(args[1]);
        const funcSuccess = args[2];
        this.send(action, parameter,'', funcSuccess);
    },
    async send(action, parameter,authPath, funcSuccess) {
        await fetch(process.env.VUE_APP_API_HOST  +authPath + "/" + action, {
        // await fetch("http://192.168.1.130:9120" + "/admin-api" + authPath, {
            body: parameter,
            headers: new Headers({
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }),
            method: 'PosT'
        }).then(response => {
            return response.text();
        }).then(text => {
            // JSON Hijacking while(1);
            let json = JSON.parse(text.replace('while(1);', ''));
            funcSuccess(json);
        });
    },
}







