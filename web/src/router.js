import Vue from 'vue'
import Router from 'vue-router'
import AdminMainPage from './views/admin/AdminMainPage.vue'
import PharmacyList from './views/admin/PharmacyList/PharmacyList.vue'
import MaskList from './views/admin/mask/MaskList.vue'
import MaskSales from './views/admin/mask/MaskSales.vue'
import MemberOrders from "./views/admin/member/MemberOrders";
import PharmacyEdit from "./views/admin/PharmacyList/PharmacyEdit";
import MemberList from "./views/admin/member/MemberList";
import SellMask from "./views/admin/mask/SellMask";


Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'AdminMainPage',
            component: AdminMainPage,
            children: [
                {path: 'pharmacy-list', name: 'PharmacyList', component: PharmacyList, meta: {pageTitle: '藥局管理'}},
                {path: 'pharmacy-edit', name: 'PharmacyEdit', component: PharmacyEdit, meta: {pageTitle: '藥局編輯'}},
                {path: 'mask-list', name: 'MaskList', component: MaskList, meta: {pageTitle: '口罩查詢'}},
                {path: 'mask-sales', name: 'MaskSales', component: MaskSales, meta: {pageTitle: '口罩銷售狀況'}},
                {path: 'sell-mask', name: 'SellMask', component: SellMask, meta: {pageTitle: '出售口罩'}},
                {path: 'member_orders', name: 'MemberOrders', component: MemberOrders, meta: {pageTitle: '會員購買狀況'}},
                {path: 'member_list', name: 'MemberList', component: MemberList, meta: {pageTitle: '會員購買狀況'}},
            ]
        }
    ]
})

