const prod = process.env.NODE_ENV === "production";

module.exports = {
    publicPath: prod ? "/" : "/",
    productionSourceMap:  !prod,
    configureWebpack: config => {
        config.externals = {
            './cptable': 'var cptable',
            '../xlsx.js': 'var _XLSX'
        }
    },
};
