package ecpay

import (
	"crypto/sha256"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

func GenerateCheckValue(MerchantID, ChoosePayment, ItemName, MerchantTradeDate, TotalAmount, TradeDesc string, MerchantTradeNo int) map[string]interface{} {
	dataList := make(map[string]interface{}, 0)
	hashKey := "cjPG0arTstZaQOTA"
	hivKey := "XW8noHBd5XdNKQzi"
	EncryptType := strconv.Itoa(1)
	PaymentType := "aio"
	ReturnURL := "https://rc-wisdom-helper.ml/api/ecpay"
	strcode := "ChoosePayment=" + ChoosePayment + "&EncryptType=" + EncryptType + "&ItemName=" + ItemName + "&MerchantID=" + MerchantID + "&MerchantTradeDate=" + MerchantTradeDate + "&MerchantTradeNo=" + strconv.Itoa(MerchantTradeNo) + "&PaymentType=" + PaymentType + "&ReturnURL=" + ReturnURL + "&TotalAmount=" + TotalAmount + "&TradeDesc=" + TradeDesc
	strcode = "HashKey=" + hashKey + "&" + strcode + "&HashIV=" + hivKey
	strcode = url.QueryEscape(strcode)
	strcode = strings.ToLower(strcode)
	strcode = strings.Replace(strcode, "%2d", "-", -1)
	strcode = strings.Replace(strcode, "%5f", "_", -1)
	strcode = strings.Replace(strcode, "%2e", ".", -1)
	strcode = strings.Replace(strcode, "%21", "!", -1)
	strcode = strings.Replace(strcode, "%2a", "*", -1)
	strcode = strings.Replace(strcode, "%28", "(", -1)
	strcode = strings.Replace(strcode, "%29", ")", -1)
	h := sha256.New()
	h.Write([]byte(strcode))
	strcode = fmt.Sprintf("%x", h.Sum(nil))
	strcode = strings.ToUpper(strcode)

	dataList["merchantID"] = MerchantID
	dataList["choosePayment"] = ChoosePayment
	dataList["itemName"] = ItemName
	dataList["tradeDate"] = MerchantTradeDate
	dataList["tradeNo"] = MerchantTradeNo
	dataList["totalAmount"] = TotalAmount
	dataList["tradeDesc"] = TradeDesc
	dataList["encryptType"] = EncryptType
	dataList["paymentType"] = PaymentType
	dataList["returnURL"] = ReturnURL
	dataList["checkMacValue"] = strcode
	return dataList
}
