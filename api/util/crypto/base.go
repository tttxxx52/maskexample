package crypto

func Base64Crypt(str string) []byte{
	if str == ""{
		return []byte("")
	}
	data:=str[7:]+str[0:7]
	return []byte(data)
}


func Base64Decrypt(byteAry []byte) string{
	if  len(byteAry) == 0 {
		return  ""
	}
	str:=string(byteAry)
	data:= str[len(str)-7:]+str[0:len(str)-7]
	return data
}
