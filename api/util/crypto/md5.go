package crypto

import (
	"api/util/crypto/web"
	"crypto/md5"
	"fmt"
)

func Md5(str string) string {
	if str == "" {
		return ""
	}
	data := []byte(str + web.Key)
	has := md5.Sum(data)
	return fmt.Sprintf("%x", has)
}

