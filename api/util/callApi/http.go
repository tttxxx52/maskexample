package callApi

import (
	"api/util/log"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func ApiWithHttp(url, method string, payload *strings.Reader) (err error, body []byte) {
	var res *http.Response
	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		return err, nil
	}
	req.Header.Add("platform", "fuCapital")
	req.Header.Add("apikey", "a05069fb-67f7-4263-899a-5ddcef65701c")
	req.Header.Add("source", "Api")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Cookie", "__cfduid=da8939c92a1bd595205b23438966f0be21614220965")
	res, err = client.Do(req)
	if err != nil {
		return err, nil
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return errors.New("http status code error"), nil
	}
	if body, err = ioutil.ReadAll(res.Body); err != nil {
		log.Error(err)
		return err, nil
	}
	fmt.Println(payload)
	return err, body

}
