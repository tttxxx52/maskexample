package mpgPay

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strings"
)

var (
	HashKey ="LJ9300ojUAhcNmFk0UMhwbbPaqCIFxL6"
	HashIV ="PMr0cqoMwB0EP8MC"
	BlockSize = 32

)

func KeyEncrypt(plaintext string) (string, string){
	bKey := []byte(HashKey)
	bIV := []byte(HashIV)
	bPlaintext := AddPKCS7Padding([]byte(plaintext), BlockSize)
	block, _ := aes.NewCipher(bKey)
	ciphertext := make([]byte, len(bPlaintext))
	mode := cipher.NewCBCEncrypter(block, bIV)
	mode.CryptBlocks(ciphertext, bPlaintext)
	hexCipherText := hex.EncodeToString(ciphertext)

	h := sha256.New()
	h.Write([]byte("HashKey="+HashKey+"&" + hexCipherText + "&HashIV="+HashIV))
	code256 := fmt.Sprintf("%x", h.Sum(nil))
	code256 = strings.ToUpper(code256)

	return hexCipherText, code256
}


func AddPKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - (len(ciphertext)%blockSize)
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}


func KeyDecrypt(deCodeText string) string{
	cipherTextDecoded, err := hex.DecodeString(deCodeText)
	if err != nil {
		panic(err)
	}
	block, err := aes.NewCipher([]byte(HashKey))
	if err != nil {
		panic(err)
	}
	mode := cipher.NewCBCDecrypter(block, []byte(HashIV))
	mode.CryptBlocks(cipherTextDecoded, cipherTextDecoded)
	return string(cipherTextDecoded)
}



func GenerateCheckValue(plaintext string) string{
	//fmt.Println("IV="+HashIV+"&" + plaintext + "&Key="+HashKey)
	h := sha256.New()
	h.Write([]byte("IV="+HashIV+"&" + plaintext + "&Key="+HashKey))
	code256 := fmt.Sprintf("%x", h.Sum(nil))
	code256 = strings.ToUpper(code256)

	return code256
}