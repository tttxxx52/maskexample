package sms

import (
	"api/util/log"
	"net/http"
	"strings"
)

const (
	// 內文長度
	LongContent   = 0
	NormalContent = 1
)

/*
	寄送簡訊
*/
func SendSMS(country ,phone string, sms string) (isOK bool, statusCode int) {
	switch country {
	case "+86":
		isOK, statusCode = SendSMSGet(country ,phone, sms)
	default:
		isOK, statusCode = SendNexmo(country ,phone, sms)

	}
	return isOK, statusCode
}

/* trim SMSGet */
func SendSMSGet(country ,phone string, sms string) (isOK bool, statusCode int)  {
	//sms get
	phoneurl := "http://sms-get.com/api_send.php?"
	userName := "Sysmar"
	password := "Sysmar"
	req, _ := http.NewRequest("GET", phoneurl, nil)
	q := req.URL.Query()
	q.Add("username", userName)
	q.Add("password", password)
	q.Add("phone", phone)
	q.Add("method", "1")
	q.Add("sms_msg", sms)
	q.Add("CharsetURL", "UTF8")
	req.URL.RawQuery = q.Encode()
	client := &http.Client{}
	response, err := client.Do(req)
	defer response.Body.Close()
	if err != nil {
		log.Error(err)
		statusCode = 404
	} else {
		if response.StatusCode == 200 {
			isOK = true
		}
		statusCode = response.StatusCode
	}

	return isOK, statusCode
}
/* trim 三竹 */
func SendMitake(country ,phone string, sms string) (isOK bool, statusCode int) {
	phoneurl := "http://smsapi.mitake.com.tw/api/mtk/SmSend"
	userName := "82838322"
	password := "83719767"
	dstaddr := phone
	encoding := "UTF8"
	smbody := sms
	req, _ := http.NewRequest("GET", phoneurl, nil)
	q := req.URL.Query()
	q.Add("username", userName)
	q.Add("password", password)
	q.Add("dstaddr", dstaddr[1:])
	q.Add("encoding", encoding)
	q.Add("smbody", smbody)
	q.Add("CharsetURL", "UTF8")
	req.URL.RawQuery = q.Encode()
	client := &http.Client{}
	response, err := client.Do(req)
	defer response.Body.Close()
	if err != nil {
		log.Error(err)
		statusCode = 404
	} else {
		if response.StatusCode == 200 {
			isOK = true
		}
		statusCode = response.StatusCode
	}

	return isOK, statusCode
}

/* trim Nexmo */
func SendNexmo(country ,phone string, sms string) (isOK bool, statusCode int) {
	url := "https://rest.nexmo.com/sms/json"
	method := "POST"
	apiKey := "c6d4b8e5"
	apiSecret := "ksXAo4d0Nfj4bfiq"
	from := "UNC"
	payload := strings.NewReader("api_key="+apiKey+"&api_secret="+apiSecret+"&to="+MergeCountryToPhone(country, phone)+"&text="+sms+"&from="+from)
	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		log.Error(err)
		return
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	if err != nil {
		log.Error(err)
		statusCode = 404
	} else {
		if res.StatusCode == 200 {
			isOK = true
		}
		statusCode = res.StatusCode
	}
	return isOK, statusCode
}

//根據國碼進行合併
func MergeCountryToPhone(CountryCode string, Phone string) string {
	var mergePhone string
	if CountryCode == "+886" {
		mergePhone = CountryCode + Phone[1:]
	} else if Phone[:1] == "0" {
		mergePhone = CountryCode + Phone[1:]
	} else {
		mergePhone = CountryCode + Phone
	}
	return mergePhone
}

//原本的電話號碼 去除國碼
func OriginalPhone(phone string) string {
	var originalPhone string
	if phone[:3] == "+886" {
		originalPhone = "0" + phone[:4]
	}
	return originalPhone
}