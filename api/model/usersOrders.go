package model

import (
	"api/database/mysql"
	"api/util"
	"api/util/log"
	"database/sql"
	"time"
)

type UsersOrders struct {
	Id          int       `table:"id"`
	UsersId     int       `table:"users_id"`
	MasksId     int       `table:"masks_id"`
	Price       float64   `table:"price"`
	Amount      int       `table:"amount"`
	CreatedTime time.Time `table:"created_time"`
}

func (model *UsersOrders) TxInsertOrdersInput(database mysql.Database, usersId, maskId, amount int, price float64, createdTime string) error {
	local, _ := time.LoadLocation("Asia/Taipei") //修改成台北時間
	inputTime, _ := time.ParseInLocation("2006-01-02 15:04:05", createdTime, local)

	model.UsersId = usersId
	model.MasksId = maskId
	model.Amount = amount
	model.Price = price
	model.CreatedTime = inputTime

	_, err := database.Model(model).Insert()
	return err
}

func (model *UsersOrders) GetUsersOrderBuyTotalList(startDate, endDate string) []map[string]interface{} {
	var usersName string
	var total float64
	var dataList = make([]map[string]interface{}, 0)
	var value []interface{}
	value = append(value, startDate, endDate)
	mysql.Model(model).
		InnerJoin("users", "users.id", "=", "users_orders.users_id").
		Select([]string{"users.name", "SUM((price * amount))  AS total"}).
		GroupBy("users_id").
		OrderBy([]string{"total"}, []string{"desc"}).
		WhereBetween("and", "users_orders.created_time", value).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&usersName, &total))
			data := map[string]interface{}{
				"name":  usersName,
				"total": total,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *UsersOrders) GetUsersOrderList() []map[string]interface{} {
	var usersName, masksName, pharmacyName string
	var price float64
	var amount int
	var createdTime time.Time
	var dataList = make([]map[string]interface{}, 0)

	mysql.Model(model).
		InnerJoin("users", "users.id", "=", "users_orders.users_id").
		InnerJoin("masks", "masks.id", "=", "users_orders.masks_id").
		InnerJoin("pharmacy", "pharmacy.id", "=", "masks.pharmacy_id").
		Select([]string{"users.name", "pharmacy.name", "masks.name", "price", "amount", "created_time"}).
		OrderBy([]string{"created_time"}, []string{"desc"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&usersName, &pharmacyName, &masksName, &price, &amount, &createdTime))
			data := map[string]interface{}{
				"usersName":    usersName,
				"pharmacyName": pharmacyName,
				"masksName":    masksName,
				"price":        price,
				"amount":       amount,
				"createdTime":  createdTime.Format("2006-01-02"),
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *UsersOrders) TxAddLog(database mysql.Database, usersId, masksId, amount int, price float64) error {
	model.UsersId = usersId
	model.MasksId = masksId
	model.Amount = amount
	model.Price = price
	model.CreatedTime = util.TimeNow()
	_, err := database.Model(model).Insert()
	return err
}

func (model *UsersOrders) GetUsersOrderListByOutputJson(usersId int) []map[string]interface{} {
	var  masksName, pharmacyName string
	var price float64
	var amount int
	var createdTime time.Time
	var dataList = make([]map[string]interface{}, 0)

	mysql.Model(model).
		InnerJoin("users", "users.id", "=", "users_orders.users_id").
		InnerJoin("masks", "masks.id", "=", "users_orders.masks_id").
		InnerJoin("pharmacy", "pharmacy.id", "=", "masks.pharmacy_id").
		Select([]string{"pharmacy.name", "masks.name", "price", "amount", "created_time"}).
		Where("users.id", "=", usersId).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan( &pharmacyName, &masksName, &price, &amount, &createdTime))
			data := map[string]interface{}{
				"pharmacyName": pharmacyName,
				"maskName":    masksName,
				"transactionAmount":        price,
				"transactionDate":  createdTime.Format("2006-01-02 15:04:05"),
			}
			dataList = append(dataList, data)
		})
	return dataList
}

