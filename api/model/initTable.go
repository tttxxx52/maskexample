package model

import (
	"api/config"
	"api/util/log"
	"database/sql"
)

func InitTable()  {
	db, err := sql.Open("mysql",
		""+config.DataBaseConfig.Username+
			":"+config.DataBaseConfig.Password+
			"@tcp("+config.DataBaseConfig.Host+
			":"+config.DataBaseConfig.Port+
			")/"+config.DataBaseConfig.DBName+"?parseTime=true&loc=Local")

	if err != nil {
		log.Error(err)
	}

	_, err = db.Exec(`
					CREATE TABLE masks (
					  id int(11) NOT NULL AUTO_INCREMENT,
					  pharmacy_id int(11) NOT NULL,
					  name varchar(255) NOT NULL,
					  price decimal(32,16) NOT NULL,
					  status int(1) NOT NULL,
					  PRIMARY KEY (id)
					) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
					`)

	_, err = db.Exec(`
					CREATE TABLE pharmacy (
						id int(11) NOT NULL AUTO_INCREMENT,
						name varchar(255) NOT NULL,
						balance decimal(32,16) NOT NULL,
						status int(1) NOT NULL,
						PRIMARY KEY (id)
					) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
					
					`)
	_, err = db.Exec(`
					CREATE TABLE pharmacy_business_hours (
					id int(11) NOT NULL AUTO_INCREMENT,
					pharmacy_id int(11) NOT NULL DEFAULT '0',
					days int(1) NOT NULL DEFAULT '0' COMMENT '星期幾  1:星期一  ~  7:星期日',
					start_time time NOT NULL DEFAULT '00:00:00',
					end_time time NOT NULL DEFAULT '00:00:00',
					is_off_day int(1) NOT NULL DEFAULT '1' COMMENT '1:正常. 2:休息',
					PRIMARY KEY (id) USING BTREE
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
					
					`)

	_, err = db.Exec(`
				CREATE TABLE users (
					id int(11) NOT NULL AUTO_INCREMENT,
					name varchar(255) NOT NULL,
					balance decimal(32,16) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
					
					`)

	_, err = db.Exec(`
					CREATE TABLE users_orders (
					id int(255) NOT NULL AUTO_INCREMENT,
					users_id int(11) NOT NULL,
					masks_id int(11) NOT NULL,
					price decimal(32,16) NOT NULL,
					amount int(11) NOT NULL,
					created_time datetime NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;`)

	_, err = db.Exec(`TRUNCATE TABLE masks`)
	_, err = db.Exec(`TRUNCATE TABLE pharmacy`)
	_, err = db.Exec(`TRUNCATE TABLE pharmacy_business_hours`)
	_, err = db.Exec(`TRUNCATE TABLE users`)
	_, err = db.Exec(`TRUNCATE TABLE users_orders`)


	log.Error(err)
	db.Close()
}
