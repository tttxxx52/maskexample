package model

import (
	"api/database/mysql"
	"api/util/log"
	"database/sql"
	"github.com/shopspring/decimal"
)

type Pharmacy struct {
	Id      int     `table:"id"`
	Name    string  `table:"name"`
	Balance float64 `table:"balance"`
	Status  int     `table:"status"`
}

func (model *Pharmacy) CreatePharmacy(name string, balance float64) (id int) {
	model.Status = 1
	model.Name = name
	model.Balance = balance
	id64, _ := mysql.Model(model).Insert()
	return int(id64)
}

func (model *Pharmacy) GetPharmacyListOption() []map[string]interface{} {
	var name string
	var dataList = make([]map[string]interface{}, 0)
	mysql.Model(model).
		Select([]string{"name"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&name))
			data := map[string]interface{}{
				"value": name,
				"label": name,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *Pharmacy) DeletePharmacy(id int) {
	model.Status = 2
	log.Error(mysql.Model(model).Where("id", "=", id).Update([]string{"status"}))
}

func (model *Pharmacy) UpdatePharmacy(id int, name string) {
	model.Name = name
	log.Error(mysql.Model(model).Where("id", "=", id).Update([]string{"name"}))
}

func (model *Pharmacy) IsExistPharmacyById(id int) bool {
	var name string
	err := mysql.Model(model).
		Where("id", "=", id).
		Select([]string{"name"}).
		Find().Scan(&name)

	if err != nil {
		return false
	} else {
		return true
	}
}

func (model *Pharmacy) GetPharmacyIdByName(name string) int {
	var id int
	log.Error(mysql.Model(model).
		Where("name", "=", name).
		Select([]string{"id"}).
		Find().Scan(&id))
	return id
}

func (model *Pharmacy) TxCalculateExchange(database mysql.Database, pharmacyId int, amount float64, transType string) error {
	//獲取錢包金額
	err := mysql.Model(model).
		Where("id", "=", pharmacyId).
		Select([]string{"balance"}).
		Find().
		Scan(&model.Balance)

	if err != nil {
		log.Error(err)
	} else {
		switch transType {
		case CostMoney:
			//----扣錢
			decimalBalance := decimal.NewFromFloat(model.Balance).Sub(decimal.NewFromFloat(amount))
			model.Balance, _ = decimalBalance.Float64()
			break
		case AddMoney:
			//----增加錢
			decimalBalance := decimal.NewFromFloat(model.Balance).Add(decimal.NewFromFloat(amount))
			model.Balance, _ = decimalBalance.Float64()
			break
		}

		err = database.Model(model).
			Where("id", "=", pharmacyId).
			Update([]string{"balance"})
	}
	return err
}

func (model *Pharmacy) GetPharmacyListByOutputJson() []map[string]interface{} {
	var name string
	var balance float64
	var id int
	var dataList = make([]map[string]interface{}, 0)
	mysql.Model(model).
		Select([]string{"id","name", "balance"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id ,&name, &balance))
			data := map[string]interface{}{
				"id":        id,
				"name":        name,
				"cashBalance": balance,
			}
			dataList = append(dataList, data)
		})
	return dataList
}
