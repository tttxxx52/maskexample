package model

import (
	"api/database/mysql"
	"api/util/log"
	"database/sql"
)

type Masks struct {
	Id         int     `table:"id"`
	PharmacyId int     `table:"pharmacy_id"`
	Name       string  `table:"name"`
	Price      float64 `table:"price"`
	Status     int     `table:"status"`
}

func (model *Masks) GetMasksInfoById(id int) map[string]interface{} {
	var name string
	var price float64
	var pharmacyId int
	log.Error(mysql.Model(model).
		Where("id", "=", id).
		Select([]string{"pharmacy_id", "name", "price"}).
		Find().Scan(&pharmacyId, &name, &price))
	return map[string]interface{}{
		"pharmacyId": pharmacyId,
		"name":       name,
		"price":      price,
	}
}

func (model *Masks) GetMasksIdByPharmacyNameAndMasksName(pharmacyName, masksName string) int {
	var id int
	log.Error(mysql.Model(model).
		InnerJoin("pharmacy", "pharmacy.id", "=", "masks.pharmacy_id").
		Where("pharmacy.name", "=", pharmacyName).
		Where("masks.name", "=", masksName).
		Select([]string{"masks.id"}).
		Find().Scan(&id))
	return id
}

/*
	filterSortOption 1 = 價錢 2 = 店名
	filterSortType 1 = 正序 2 = 倒敘
*/
func (model *Masks) GetMaskList(filterKey, filterPharmacyName string, filterSortOption, filterSortType int, priceMin, priceMax float64) []map[string]interface{} {
	var dataList = make([]map[string]interface{}, 0)
	var id int
	var pharmacyName, maskName string
	var price float64
	var table = mysql.Model(model)

	if priceMin != 0 {
		table.Where("masks.price", ">=", priceMin)
	}

	if priceMax != 0 {
		table.Where("masks.price", "<=", priceMax)
	}

	if filterPharmacyName != "" {
		table.Where("pharmacy.name", "=", filterPharmacyName)
	}
	if filterSortOption != 0 && filterSortType != 0 {
		var sort string
		if filterSortType == 1 {
			sort = "desc"
		} else {
			sort = "asc"
		}

		if filterSortOption == 1 {
			table.OrderBy([]string{"price"}, []string{sort})
		} else {
			table.OrderBy([]string{"pharmacy.name"}, []string{sort})
		}
	}

	if filterKey != "" {
		table.Where("pharmacy.name", "like", "%"+filterKey+"%")
		table.WhereOr("masks.name", "like", "%"+filterKey+"%")
	}

	table.
		InnerJoin("pharmacy", "pharmacy.id", "=", "masks.pharmacy_id").
		Select([]string{"masks.id", "pharmacy.name", "masks.name", "price"}).
		Where("status", "=", 1).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &pharmacyName, &maskName, &price))
			data := map[string]interface{}{
				"id":           id,
				"pharmacyName": pharmacyName,
				"maskName":     maskName,
				"price":        price,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

/*
	獲取銷售列表
*/
func (model *Masks) GetMaskSales(startDate, endDate string) []map[string]interface{} {
	var maskName string
	var saleTotal float64
	var dataList = make([]map[string]interface{}, 0)

	mysql.NewDB().Option(func(database mysql.Database) {
		var value []interface{}
		value = append(value, startDate, endDate)
		sqlStr := `SELECT name , total FROM masks JOIN (
				SELECT
					masks_id,
					SUM((price * amount))  AS total 
				FROM
					users_orders
				WHERE created_time BETWEEN ? AND ?
					GROUP BY  masks_id
					) usersOrders ON usersOrders.masks_id = masks.id`
		rows, _ := database.Query(sqlStr, value...)
		defer database.CloseRows(rows)
		for rows.Next() {
			log.Error(rows.Scan(&maskName, &saleTotal))
			data := map[string]interface{}{
				"maskName":  maskName,
				"saleTotal": saleTotal,
			}
			dataList = append(dataList, data)
		}
	})
	return dataList
}

func (model *Masks) GetMaskListByPharmacyId(pharmacyId int) []map[string]interface{} {
	var id int
	var name string
	var price float64
	var dataList = make([]map[string]interface{}, 0)

	mysql.Model(model).
		Where("pharmacy_id", "=", pharmacyId).
		Where("status", "=", 1).
		Select([]string{"id", "name", "price"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &name, &price))
			data := map[string]interface{}{
				"id":    id,
				"name":  name,
				"price": price,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *Masks) DeleteMasks(id int) {
	model.Status = 2
	log.Error(mysql.Model(model).Where("id", "=", id).Update([]string{"status"}))
}

func (model *Masks) UpdateMasks(id int, name string, price float64) {
	model.Price = price
	model.Name = name
	log.Error(mysql.Model(model).Where("id", "=", id).Update([]string{"name", "price"}))
}

func (model *Masks) CreateMasks(pharmacyId int , name string, price float64)  {
	model.Status = 1
	model.Name = name
	model.Price = price
	model.PharmacyId = pharmacyId
	mysql.Model(model).Insert()
}

func (model *Masks) IsExistMasksById(id int) bool {
	var name string
	err := mysql.Model(model).
		Where("id", "=", id).
		Select([]string{"name"}).
		Find().Scan(&name)

	if err != nil {
		return false
	} else {
		return true
	}
}

func (model *Masks) GetMasksListOptionByPharmacyId(pharmacyId int) []map[string]interface{} {
	var name string
	var id int
	var dataList = make([]map[string]interface{}, 0)
	mysql.Model(model).
		Select([]string{"id", "name"}).
		Where("pharmacy_id", "=", pharmacyId).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &name))
			data := map[string]interface{}{
				"value": id,
				"label": name,
			}
			dataList = append(dataList, data)
		})
	return dataList
}


func (model *Masks) GetMaskListByOutputJson(pharmacyId int) []map[string]interface{} {
	var id int
	var name string
	var price float64
	var dataList = make([]map[string]interface{}, 0)

	mysql.Model(model).
		Where("pharmacy_id", "=", pharmacyId).
		Where("status", "=", 1).
		Select([]string{"id", "name", "price"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &name, &price))
			data := map[string]interface{}{
				"name":  name,
				"price": price,
			}
			dataList = append(dataList, data)
		})
	return dataList
}