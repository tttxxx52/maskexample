package model

import (
	"api/database/mysql"
	"api/util/log"
	"database/sql"
	"github.com/shopspring/decimal"
)

type Users struct {
	Id      int     `table:"id"`
	Name    string  `table:"name"`
	Balance float64 `table:"balance"`
}

const (
	//----扣款
	CostMoney = "扣款"

	//----增加錢
	AddMoney = "獲得"
)

func (model *Users) GetUsersIdByName(name string) int {
	var id int
	log.Error(mysql.Model(model).
		Where("name", "=", name).
		Select([]string{"id"}).
		Find().Scan(&id))
	return id
}

func (model *Users) GetUsersInfoById(id int) map[string]interface{} {
	var name string
	var balance float64
	log.Error(mysql.Model(model).
		Where("id", "=", id).
		Select([]string{"name", "balance"}).
		Find().Scan(&name, &balance))
	return map[string]interface{}{
		"name":    name,
		"balance": balance,
	}
}

func (model *Users) GetMemberListOption() []map[string]interface{} {
	var name string
	var id int
	var dataList = make([]map[string]interface{}, 0)
	mysql.Model(model).
		Select([]string{"id", "name"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &name))
			data := map[string]interface{}{
				"value": id,
				"label": name,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *Users) GetMemberList(filterName string) []map[string]interface{} {
	var name string
	var balance float64
	var dataList = make([]map[string]interface{}, 0)
	table := mysql.Model(model)

	if filterName != "" {
		table.Where("name", "like", "%"+filterName+"%")
	}

	table.
		Select([]string{"name", "balance"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&name, &balance))
			data := map[string]interface{}{
				"name":    name,
				"balance": balance,
			}
			dataList = append(dataList, data)
		})
	return dataList
}

func (model *Users) TxCalculateExchange(database mysql.Database, userId int, amount float64, transType string) error {
	//獲取錢包金額
	err := mysql.Model(model).
		Where("id", "=", userId).
		Select([]string{"balance"}).
		Find().
		Scan(&model.Balance)

	if err != nil {
		log.Error(err)
	} else {
		switch transType {
		case CostMoney:
			//----扣錢
			decimalBalance := decimal.NewFromFloat(model.Balance).Sub(decimal.NewFromFloat(amount))
			model.Balance, _ = decimalBalance.Float64()
			break
		case AddMoney:
			//----增加錢
			decimalBalance := decimal.NewFromFloat(model.Balance).Add(decimal.NewFromFloat(amount))
			model.Balance, _ = decimalBalance.Float64()
			break
		}

		err = database.Model(model).
			Where("id", "=", userId).
			Update([]string{"balance"})
	}
	return err
}

func (model *Users) TxCreateUsers(database mysql.Database, name string, balance float64) error {
	model.Name = name
	model.Balance = balance
	_, err := database.Model(model).Insert()
	return err
}

func (model *Users) GetMemberListByOutputJson() []map[string]interface{} {
	var name string
	var balance float64
	var id int
	var dataList = make([]map[string]interface{}, 0)
	table := mysql.Model(model)

	table.
		Select([]string{"id","name", "balance"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id ,&name, &balance))
			data := map[string]interface{}{
				"id":              id,
				"name":              name,
				"cashBalance":       balance,
			}
			dataList = append(dataList, data)
		})
	return dataList
}
