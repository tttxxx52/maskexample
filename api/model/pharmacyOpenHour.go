package model

import (
	"api/database/mysql"
	"api/util/log"
	"database/sql"
)

type PharmacyBusinessHours struct {
	Id         int    `table:"id"`
	PharmacyId int    `table:"pharmacy_id"`
	Days       int    `table:"days"`
	StartTime  string `table:"start_time"`
	EndTime    string `table:"end_time"`
	IsOffDay   int    `table:"is_off_day"`
}

func (model *PharmacyBusinessHours) GetPharmacyBusinessHoursList(filterDay int) []map[string]interface{} {
	var pharmacyList = make(map[string]map[string]interface{}, 0)
	var dataList = make([]map[string]interface{}, 0)
	var id, days, isOffDay, pharmacyId int
	var startTime, endTime, pharmacyName string
	var balance float64
	var pharmacy Pharmacy
	var pharmacySql = mysql.Model(&pharmacy)
	var PharmacyBusinessHoursSql = mysql.Model(model)
	var pharmacyAryId = make([]interface{}, 0)

	//查詢哪些藥局星期幾有開 並加入之後查詢的where內
	if filterDay != 0 {
		mysql.Model(model).
			Where("is_off_day", "=", 1).
			Where("days", "=", filterDay).
			Select([]string{"pharmacy_id"}).
			Get(func(rows *sql.Rows) {
				log.Error(rows.Scan(&pharmacyId))
				pharmacyAryId = append(pharmacyAryId, pharmacyId)
			})
		pharmacySql.WhereIn("and", "id", pharmacyAryId)
		PharmacyBusinessHoursSql.WhereIn("and", "pharmacy_business_hours.pharmacy_id", pharmacyAryId)
	}

	pharmacySql.
		Select([]string{"id", "name", "balance"}).
		Where("status", "=", 1).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &pharmacyName, &balance))
			pharmacyList[pharmacyName] = map[string]interface{}{
				"id":            id,
				"balance":       balance,
				"businessHours": []map[string]interface{}{},
			}
		})

	PharmacyBusinessHoursSql.
		InnerJoin("pharmacy", "pharmacy.id", "=", "pharmacy_business_hours.pharmacy_id").
		Where("pharmacy.status", "=", 1).
		Select([]string{"pharmacy.id", "pharmacy.name", "days", "start_time", "end_time", "is_off_day"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&id, &pharmacyName, &days, &startTime, &endTime, &isOffDay))
			data := map[string]interface{}{
				"days":      days,
				"startTime": startTime[:len(startTime)-3],
				"endTime":   endTime[:len(endTime)-3],
				"isOffDay":  isOffDay,
			}
			pharmacyList[pharmacyName]["businessHours"] = append(pharmacyList[pharmacyName]["businessHours"].([]map[string]interface{}), data)
		})

	//處理資料格式
	for key, value := range pharmacyList {
		data := map[string]interface{}{
			"name":          key,
			"id":            value["id"],
			"balance":       value["balance"],
			"businessHours": value["businessHours"],
		}
		dataList = append(dataList, data)
	}

	return dataList
}

func (model *PharmacyBusinessHours) CreatePharmacyBusinessHours(pharmacyId int) {
	mysql.NewDB().Transaction(func(database mysql.Database) {
		for i := 1; i <= 7; i++ {
			model.PharmacyId = pharmacyId
			model.IsOffDay = 2
			model.Days = i
			database.Model(model).Insert()
		}
		log.Error(database.Commit())
	})
}

func (model *PharmacyBusinessHours) UpdatePharmacyBusinessHoursByJsonInput(pharmacyId, days int, startTime, endTime string) {
	model.StartTime = startTime
	model.EndTime = endTime
	model.IsOffDay = 1
	log.Error(mysql.Model(model).
		Where("pharmacy_id", "=", pharmacyId).
		Where("days", "=", days).
		Update([]string{"start_time", "end_time", "is_off_day"}))
}

func (model *PharmacyBusinessHours) GetPharmacyBusinessHoursByOutputJson(pharmacyId int) string {
	var day int
	var startTime, endTime string
	var data = map[string][]int{}
	var returnStr string

	mysql.Model(model).
		Where("pharmacy_id", "=", pharmacyId).
		Where("is_off_day", "=", 1).
		Select([]string{"days", "start_time", "end_time"}).
		OrderBy([]string{"days"}, []string{"asc"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&day, &startTime, &endTime))
			key := startTime[:5] + "-" + endTime[:5]
			data[key] = append(data[key], day)
		})

	for key, value := range data {
		if len(value) == 1 { //只有一筆資料
			if len(returnStr) > 0 {
				if returnStr[len(returnStr)-1:]  != "/" {
					returnStr += ","
				}
			}

			returnStr += dayStrToDayInt(value[0]) + " " + key + " /"
		} else if len(value) > 1 { //兩筆一上資料
			isContinuous := false
			isContinuousOver := false
			var continuousOen = []int{}
			var continuousTwo = []int{}
			var disContinuousDay = []int{}
			for i := 0; i < len(value)-1; i++ {
				if value[i+1] - value[i] == 1 { //判斷是否連率
					isContinuous = true

					if isContinuous && isContinuousOver { //是否已經有中止連續的陣列
						continuousTwo = append(continuousTwo, value[i+1])
					} else {
						continuousOen = append(continuousOen, value[i+1])
					}

					if i == 0 {
						continuousOen = append(continuousOen, value[i])
					}
				} else {
					if isContinuous {
						isContinuousOver = true
					}

					disContinuousDay = append(disContinuousDay, value[i+1])
					if i == 0 {
						disContinuousDay = append(disContinuousDay, value[i])
					}
				}
			}
			//不連續
			for _, day := range disContinuousDay {
				if len(returnStr) > 0 {
					if returnStr[len(returnStr)-1:]  != "/" {
						returnStr += ","
					}
				}
				returnStr += dayStrToDayInt(day)
			}
			//連續1
			if len(continuousOen) > 0 {
				maxDay := max(continuousOen)
				minDay := min(continuousOen)

				if len(returnStr) > 0 {
					if returnStr[len(returnStr)-1:]  != "/" {
						returnStr += ","
					}
				}
				returnStr += dayStrToDayInt(minDay) + "-" + dayStrToDayInt(maxDay)
			}
			//連續2
			if len(continuousTwo) > 0 {
				maxDay := max(continuousTwo)
				minDay := min(continuousTwo)
				if len(returnStr) > 0 {
					if returnStr[len(returnStr)-1:]  != "/" {
						returnStr += ","
					}
				}
				returnStr += dayStrToDayInt(minDay) + "-" + dayStrToDayInt(maxDay)
			}
			returnStr += " " +  key +" /"
		}
	}
	return returnStr[:len(returnStr)-1]
}

func dayStrToDayInt(day int) string {
	switch day {
	case 1:
		return "Mon"
	case 2:
		return "Tue"
	case 3:
		return "Wed"
	case 4:
		return "Thu"
	case 5:
		return "Fri"
	case 6:
		return "Sat"
	case 7:
		return "Sun"
	default:
		return ""
	}
}

func max(l []int) (max int) {
	if len(l) == 0{
		return 0
	}

	max = l[0]
	for _, v := range l {
		if v > max {
			max = v
		}
	}
	return
}

func min(l []int) (min int) {
	if len(l) == 0{
		return
	}
	min = l[0]
	for _, v := range l {
		if v < min {
			min = v
		}
	}
	return
}
