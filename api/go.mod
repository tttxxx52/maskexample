module api

go 1.13

require (
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/Shopify/sarama v1.24.1
	github.com/dgryski/dgoogauth v0.0.0-20190221195224-5a805980a5f3
	github.com/disintegration/imaging v1.6.2
	github.com/garyburd/redigo v1.6.0
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/joho/godotenv v1.3.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.8.1
	github.com/shopspring/decimal v1.2.0
	github.com/tidwall/pretty v1.0.2 // indirect
	github.com/zeekay/gochimp3 v0.0.0-20191207050959-1023e7b1d6cf
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20200927032502-5d4f70055728 // indirect
	golang.org/x/text v0.3.3
	google.golang.org/appengine v1.6.5 // indirect
)
