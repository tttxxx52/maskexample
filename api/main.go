package main

import (
	"api/config"
	"api/database/mysql"
	"api/handler"
	"api/util/log"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env")
	if err == nil {
		gin.SetMode(gin.ReleaseMode)
		config.InitConfig()
	}

	//DataBase Pool
	mysql.OpenConnect()

	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "POST"},
		AllowHeaders:    []string{"Content-Type", "Auth-Token", "Csrf-Token", "Access-Control-Allow-Origin"},
	}))

	apiRouter := router.Group("/api")
	apiRouter.POST("/getPharmacyInfo", handler.GetPharmacyInfo) //獲取藥局資訊
	apiRouter.POST("/getPharmacyListOption", handler.GetPharmacyListOption)
	apiRouter.POST("/deletePharmacy", handler.DeletePharmacy) //刪除藥局
	apiRouter.POST("/updatePharmacy", handler.UpdatePharmacy) //編輯藥局資訊

	apiRouter.POST("/getMaskList", handler.GetMaskList)                         //獲取口罩列表
	apiRouter.POST("/getMaskSales", handler.GetMaskSales)                       //獲取銷售狀況
	apiRouter.POST("/getMaskListByPharmacyId", handler.GetMaskListByPharmacyId) //口罩清單by藥局
	apiRouter.POST("/deleteMasks", handler.DeleteMasks)                         //刪除口罩
	apiRouter.POST("/updateMasks", handler.UpdateMasks)                         //編輯口罩資訊
	apiRouter.POST("/sellMasks", handler.SellMasks)                             //出售口罩
	apiRouter.POST("/getMaskListOptionByPharmacyName", handler.GetMaskListOptionByPharmacyName)

	apiRouter.POST("/getUsersOrderBuyTotalList", handler.GetUsersOrderBuyTotalList) //使用者購買總額
	apiRouter.POST("/getUsersOrderList", handler.GetUsersOrderList)                 //使用者購買記錄
	apiRouter.POST("/getMemberList", handler.GetMemberList)                         //會員列表
	apiRouter.POST("/getMemberListOption", handler.GetMemberListOption)

	apiRouter.POST("/initDataFromJson", handler.InitDataFromJson)
	apiRouter.POST("/outputUsersJson", handler.OutputUsersJson)
	apiRouter.POST("/outputPharmacyJson", handler.OutputPharmacyJson)



	fmt.Println(config.ServerConfig.Port)
	log.Error(router.Run(":" + config.ServerConfig.Port))

}
