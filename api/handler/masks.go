package handler

import (
	"api/database/mysql"
	"api/model"
	"api/util"
	"api/util/log"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

/*獲取口罩列表by pharmacyId*/
func GetMaskListByPharmacyId(c *gin.Context) {
	type ReceivedData struct {
		Id int `json:"id"`
	}
	var data ReceivedData
	var masks model.Masks
	var pharmacy model.Pharmacy
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if !pharmacy.IsExistPharmacyById(data.Id) {
		c.JSON(http.StatusBadRequest, util.RS{Message: "id is not find", Status: false})
	} else {
		c.JSON(http.StatusOK, masks.GetMaskListByPharmacyId(data.Id))
	}
}

/*
	獲取口罩列表
*/
func GetMaskList(c *gin.Context) {
	type ReceivedData struct {
		FilterPharmacyName string  `json:"filterPharmacyName"`
		FilterSortOption   int     `json:"filterSortOption"`
		FilterSortType     int     `json:"filterSortType"`
		FilterKey          string  `json:"filterKey"`
		PriceMin           float64 `json:"priceMin"`
		PriceMax           float64 `json:"priceMax"`
	}
	var data ReceivedData
	var masks model.Masks

	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, "parameters error")
	} else if data.FilterSortOption > 2 && data.FilterSortOption < 0 {
		c.JSON(http.StatusBadRequest, "parameters error")
	} else if data.FilterSortType > 2 && data.FilterSortType < 0 {
		c.JSON(http.StatusBadRequest, "parameters error")
	} else {
		c.JSON(http.StatusOK, masks.GetMaskList(data.FilterKey, data.FilterPharmacyName, data.FilterSortOption, data.FilterSortType, data.PriceMin, data.PriceMax))
	}
}

/*
	獲取銷售列表
*/
func GetMaskSales(c *gin.Context) {
	type ReceivedData struct {
		StartDate string `json:"startDate"`
		EndDate   string `json:"endDate"`
	}
	var data ReceivedData
	var masks model.Masks
	var timeErr1, timeErr2 error

	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else {
		//驗證日期格式是否正確
		if data.StartDate != "" {
			_, timeErr1 = time.ParseInLocation("2006-01-02 15:04:05", data.StartDate+" 00:00:00", time.Local)
			data.StartDate += " 00:00:01"
		} else {
			data.StartDate = "0001-01-01 00:00:01"
		}
		if data.EndDate != "" {
			_, timeErr2 = time.ParseInLocation("2006-01-02 15:04:05", data.EndDate+" 23:59:59", time.Local)
			data.EndDate += " 23:59:59"
		} else {
			data.EndDate = "9999-12-31 23:59:59"
		}
		if timeErr1 != nil || timeErr2 != nil {
			c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
		} else {
			c.JSON(http.StatusOK, masks.GetMaskSales(data.StartDate, data.EndDate))
		}
	}
}

func SellMasks(c *gin.Context) {
	type ReceivedData struct {
		UsersId int `json:"usersId"`
		MasksId int `json:"masksId"`
		Amount  int `json:"amount"`
	}
	var data ReceivedData
	var masks model.Masks
	var users model.Users
	var pharmacy model.Pharmacy
	var usersOrders model.UsersOrders
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if data.UsersId == 0 || data.MasksId == 0 || data.Amount == 0 {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else {
		//獲取masks資訊
		masksInfo := masks.GetMasksInfoById(data.MasksId)
		//獲取使用者資訊
		usersInfo := users.GetUsersInfoById(data.UsersId)
		//判斷使用者是否有錢可以購買
		if usersInfo["balance"].(float64) < masksInfo["price"].(float64)*float64(data.Amount) {
			c.JSON(http.StatusBadRequest, util.RS{Message: "使用者金額不足", Status: false})
		} else {
			mysql.NewDB().Transaction(func(database mysql.Database) {
				//cost balance
				costErr := users.TxCalculateExchange(database, data.UsersId, masksInfo["price"].(float64)*float64(data.Amount), model.CostMoney)
				//add balance
				addErr := pharmacy.TxCalculateExchange(database, masksInfo["pharmacyId"].(int), masksInfo["price"].(float64)*float64(data.Amount), model.AddMoney)
				//add log
				logErr := usersOrders.TxAddLog(database, data.UsersId, data.MasksId, data.Amount, masksInfo["price"].(float64))

				if costErr == nil && addErr == nil && logErr == nil {
					log.Error(database.Commit())
					c.JSON(http.StatusOK, util.RS{Message: "出售成功", Status: false})
				} else {
					log.Error(database.Rollback())
					c.JSON(http.StatusBadRequest, util.RS{Message: "system error", Status: false})
				}
			})
		}
	}
}

func DeleteMasks(c *gin.Context) {
	type ReceivedData struct {
		Id int `json:"id"`
	}
	var data ReceivedData
	var masks model.Masks
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if !masks.IsExistMasksById(data.Id) {
		c.JSON(http.StatusBadRequest, util.RS{Message: "id is not find", Status: false})
	} else {
		masks.DeleteMasks(data.Id)
		c.JSON(http.StatusOK, util.RS{Message: "刪除成功", Status: true})
	}
}

func UpdateMasks(c *gin.Context) {
	type ReceivedData struct {
		Id    int     `json:"id"`
		Name  string  `json:"name"`
		Price float64 `json:"price"`
	}
	var data ReceivedData
	var masks model.Masks
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if data.Name == "" || data.Price == 0 {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if !masks.IsExistMasksById(data.Id) {
		c.JSON(http.StatusBadRequest, util.RS{Message: "id is not find", Status: false})
	} else {
		masks.UpdateMasks(data.Id, data.Name, data.Price)
		c.JSON(http.StatusOK, util.RS{Message: "更新成功", Status: true})
	}
}

func GetMaskListOptionByPharmacyName(c *gin.Context) {
	type ReceivedData struct {
		Name string `json:"name"`
	}
	var data ReceivedData
	var pharmacy model.Pharmacy
	var masks model.Masks
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if pharmacyId := pharmacy.GetPharmacyIdByName(data.Name); pharmacyId == 0 {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else {
		c.JSON(http.StatusOK, masks.GetMasksListOptionByPharmacyId(pharmacyId))
	}
}
