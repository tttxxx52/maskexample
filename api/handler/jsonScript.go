package handler

import (
	"api/database/mysql"
	"api/model"
	"api/util"
	"api/util/log"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strings"
)

func InitDataFromJson(c *gin.Context) {
	model.InitTable()
	InputPharmaciesJson()
	InputUsersJson()
	c.JSON(http.StatusOK, util.RS{Message: "資料初始化完成", Status: true})
}



func InputPharmaciesJson() {
	type Masks struct {
		Name  string  `json:"name"`
		Price float64 `json:"price"`
	}

	type dataReceived struct {
		Name         string  `json:"name"`
		CashBalance  float64 `json:"cashBalance"`
		OpeningHours string  `json:"openingHours"`
		Masks        []Masks `json:"masks"`
	}
	var data []dataReceived
	var pharmacy model.Pharmacy
	var pharmacyBusinessHours model.PharmacyBusinessHours
	var masks model.Masks
	var jsonStr []byte
	switch gin.Mode() {
	case gin.ReleaseMode:
		jsonStr, _ = ioutil.ReadFile("/var/www/html/jsonFile/pharmacies.json")

	case gin.DebugMode:
		jsonStr, _ = ioutil.ReadFile("jsonFile/pharmacies.json")

	case gin.TestMode:
		jsonStr, _ = ioutil.ReadFile("jsonFile/pharmacies.json")

	}

	if err := json.Unmarshal(jsonStr, &data); err != nil {
		log.Error(err)
	} else {
		for _, value := range data {
			//建立藥局
			pharmacyId := pharmacy.CreatePharmacy(value.Name, value.CashBalance)
			//建立營業時間
			pharmacyBusinessHours.CreatePharmacyBusinessHours(pharmacyId)
			//建立口罩
			for _, masksInfo := range value.Masks {
				masks.CreateMasks(pharmacyId, masksInfo.Name, masksInfo.Price)
			}

			//修改營業時間
			value.OpeningHours = strings.Replace(value.OpeningHours, " ", "", -1)
			openHourAry := strings.Split(value.OpeningHours, "/")
			for _, openHourStr := range openHourAry {
				var isContinuousParse = false
				var dayAry []int
				openHour := openHourStr[len(openHourStr)-11:]
				openDay := openHourStr[:len(openHourStr)-11]

				//解析不連續的天數字串
				discontinuousDayIndex := strings.Index(openDay, ",")
				continuousDayIndex := strings.Index(openDay, "-")

				if discontinuousDayIndex > -1 {
					dayStrAry := strings.Split(openDay, ",")
					for _, dayStr := range dayStrAry {
						//再次查看是否有連續時間
						isContinuousDayIndex := strings.Index(dayStr, "-")
						if isContinuousDayIndex > -1 { //擁有連續時間,做解析
							dayStrAry := strings.Split(openDay, "-")
							dayStart := 0
							dayEnd := 0
							//建立開始與結束
							for index, dayStr := range dayStrAry {
								day := getDayIntByStr(dayStr)
								if index == 0 {
									dayStart = day
								} else {
									dayEnd = day
								}
							}
							//加入陣列
							for dayStart <= dayEnd {
								dayAry = append(dayAry, dayStart)
								dayStart++
							}
							isContinuousParse = true
						} else {
							day := getDayIntByStr(dayStr)
							dayAry = append(dayAry, day)
						}
					}
				} else if continuousDayIndex > -1 && isContinuousParse == false {
					dayStrAry := strings.Split(openDay, "-")
					dayStart := 0
					dayEnd := 0
					//建立開始與結束
					for index, dayStr := range dayStrAry {
						day := getDayIntByStr(dayStr)
						if index == 0 {
							dayStart = day
						} else {
							dayEnd = day
						}
					}
					//加入陣列
					for dayStart <= dayEnd {
						dayAry = append(dayAry, dayStart)
						dayStart++
					}
				} else {
					day := getDayIntByStr(openDay)
					dayAry = append(dayAry, day)
				}
				//更新時間
				for _, day := range dayAry {
					pharmacyBusinessHours.UpdatePharmacyBusinessHoursByJsonInput(pharmacyId, day, openHour[:5], openHour[6:])
				}
			}
		}
		fmt.Println("success")
	}
}

func InputUsersJson() {
	var jsonStr []byte

	switch gin.Mode() {
	case gin.ReleaseMode:
		jsonStr, _ = ioutil.ReadFile("/var/www/html/jsonFile/users.json")

	case gin.DebugMode:
		jsonStr, _ = ioutil.ReadFile("jsonFile/users.json")

	case gin.TestMode:
		jsonStr, _ = ioutil.ReadFile("jsonFile/users.json")

	}

	type PurchaseHistoriesStruct struct {
		PharmacyName      string  `json:"pharmacyName"`
		MaskName          string  `json:"maskName"`
		TransactionAmount float64 `json:"transactionAmount"`
		TransactionDate   string  `json:"transactionDate"`
	}

	type dataReceived struct {
		Name              string                    `json:"name"`
		CashBalance       float64                   `json:"cashBalance"`
		PurchaseHistories []PurchaseHistoriesStruct `json:"purchaseHistories"`
	}
	var users model.Users
	var masks model.Masks
	var usersOrders model.UsersOrders
	var data []dataReceived
	var insertStatus = true
	if err := json.Unmarshal(jsonStr, &data); err != nil {
		log.Error(err)
	} else {
		//新增usersId
		mysql.NewDB().Transaction(func(database mysql.Database) {
			var isSuccess = true
			for _, value := range data {
				err := users.TxCreateUsers(database, value.Name, value.CashBalance)
				if err != nil {
					isSuccess = false
					break
				}
			}
			if isSuccess {
				log.Error(database.Commit())
			} else {
				log.Error(database.Rollback())
			}
		})

		//Transaction
		mysql.NewDB().Transaction(func(database mysql.Database) {
			for _, value := range data {
				usersId := users.GetUsersIdByName(value.Name)
				if usersId == 0 {
					insertStatus = false
					break
				}

				for _, purchaseHistories := range value.PurchaseHistories {
					masksId := masks.GetMasksIdByPharmacyNameAndMasksName(purchaseHistories.PharmacyName, purchaseHistories.MaskName)
					if masksId == 0 {
						insertStatus = false
						break
					}

					err := usersOrders.TxInsertOrdersInput(database, usersId, masksId, 1, purchaseHistories.TransactionAmount, purchaseHistories.TransactionDate)
					if err != nil {
						insertStatus = false
						break
					}
				}

				if !insertStatus {
					break
				}
			}

			if insertStatus {
				fmt.Println("success")
				log.Error(database.Commit())
			} else {
				log.Error(database.Rollback())
			}
		})
	}
}


func OutputUsersJson(c *gin.Context) {
	var users model.Users
	var usersOrders model.UsersOrders
	usersList:=users.GetMemberListByOutputJson()
	//沒時間做字典先用資料庫查
	for _ , users :=range usersList{
		users["purchaseHistories"] = usersOrders.GetUsersOrderListByOutputJson(users["id"].(int))
	}

	c.JSON(http.StatusOK, usersList)
}



func OutputPharmacyJson(c *gin.Context) {
	var pharmacy model.Pharmacy
	var pharmacyBusinessHours model.PharmacyBusinessHours
	var masks model.Masks
	var data = map[string]interface{}{
		"name" :"",
		"cashBalance" :0.0,
		"openingHours" :"",
		"masks" :make([]map[string]interface{},0),
	}
	var dataList = make([]map[string]interface{},0)

	pharmacyList :=pharmacy.GetPharmacyListByOutputJson()

	for _ , value := range pharmacyList {
		value["openingHours"] = pharmacyBusinessHours.GetPharmacyBusinessHoursByOutputJson(value["id"].(int))
		value["masks"] = masks.GetMaskListByOutputJson(value["id"].(int))
	}

	//整理
	for _ , value := range pharmacyList {
		data["name"] = value["name"]
		data["cashBalance"] = value["cashBalance"]
		data["openingHours"] = value["openingHours"]
		data["masks"] = value["masks"]
		dataList = append(dataList, data)
	}

	c.JSON(http.StatusOK, dataList)
}


func getDayIntByStr(dayStr string) int {
	switch dayStr {
	case "Mon":
		return 1
	case "Tue":
		return 2
	case "Wed":
		return 3
	case "Thu":
		return 4
	case "Fri":
		return 5
	case "Sat":
		return 6
	case "Sun":
		return 7
	default:
		return 0
	}
}
