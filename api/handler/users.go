package handler

import (
	"api/model"
	"api/util"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func GetMemberList(c *gin.Context) {
	type ReceivedData struct {
		FilterName string `json:"filterName"`
	}
	var data ReceivedData
	var users model.Users
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, "parameters error")
	} else {
		c.JSON(http.StatusOK, users.GetMemberList(data.FilterName))
	}
}

func GetMemberListOption(c *gin.Context) {
	var users model.Users
	c.JSON(http.StatusOK, users.GetMemberListOption())

}

/*
	獲取使用者購買狀況
*/
func GetUsersOrderBuyTotalList(c *gin.Context) {
	type ReceivedData struct {
		StartDate string `json:"startDate"`
		EndDate   string `json:"endDate"`
	}
	var data ReceivedData
	var usersOrders model.UsersOrders
	var timeErr1, timeErr2 error

	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else {
		//驗證日期格式是否正確
		if data.StartDate != "" {
			_, timeErr1 = time.ParseInLocation("2006-01-02 15:04:05", data.StartDate+" 00:00:00", time.Local)
			data.StartDate += " 00:00:01"
		} else {
			data.StartDate = "0001-01-01 00:00:01"
		}
		if data.EndDate != "" {
			_, timeErr2 = time.ParseInLocation("2006-01-02 15:04:05", data.EndDate+" 23:59:59", time.Local)
			data.EndDate += " 23:59:59"
		} else {
			data.EndDate = "9999-12-31 23:59:59"
		}
		if timeErr1 != nil || timeErr2 != nil {
			c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
		} else {
			c.JSON(http.StatusOK, usersOrders.GetUsersOrderBuyTotalList(data.StartDate, data.EndDate))
		}
	}
}

func GetUsersOrderList(c *gin.Context) {
	type ReceivedData struct{}
	var data ReceivedData
	var usersOrders model.UsersOrders

	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else {
		c.JSON(http.StatusOK, usersOrders.GetUsersOrderList())
	}
}
