package handler

import (
	"api/model"
	"api/util"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func GetPharmacyInfo(c *gin.Context) {
	type ReceivedData struct {
		FilterDate string `json:"filterDate"`
		FilterDay  int    `json:"filterDay"`
	}
	var data ReceivedData
	var pharmacyBusinessHours model.PharmacyBusinessHours
	var day int
	var timeNow time.Time

	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else {
		//獲取星期幾
		if data.FilterDate != "" {
			timeNow, err = time.ParseInLocation("2006-01-02 15:04:05", data.FilterDate+" 00:00:00", time.Local)
			if err == nil {
				day = int(timeNow.Weekday())
			}
		}

		if err != nil {
			c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
		} else {
			if data.FilterDay != 0 {
				day = data.FilterDay
			}

			data := pharmacyBusinessHours.GetPharmacyBusinessHoursList(day)
			c.JSON(http.StatusOK, data)
		}
	}
}

func GetPharmacyListOption(c *gin.Context) {
	var pharmacy model.Pharmacy
	c.JSON(http.StatusOK, pharmacy.GetPharmacyListOption())
}

func DeletePharmacy(c *gin.Context) {
	type ReceivedData struct {
		Id int `json:"id"`
	}
	var data ReceivedData
	var pharmacy model.Pharmacy
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if !pharmacy.IsExistPharmacyById(data.Id) {
		c.JSON(http.StatusBadRequest, util.RS{Message: "id is not find", Status: false})
	} else {
		pharmacy.DeletePharmacy(data.Id)
		c.JSON(http.StatusOK, util.RS{Message: "刪除成功", Status: true})
	}
}

func UpdatePharmacy(c *gin.Context) {
	type ReceivedData struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	}
	var data ReceivedData
	var pharmacy model.Pharmacy
	if err := c.BindJSON(&data); err != nil {
		c.JSON(http.StatusBadRequest, util.RS{Message: "parameters error", Status: false})
	} else if !pharmacy.IsExistPharmacyById(data.Id) {
		c.JSON(http.StatusBadRequest, util.RS{Message: "id is not find", Status: false})
	} else if data.Name == "" {
		c.JSON(http.StatusBadRequest, util.RS{Message: "id is not find", Status: false})
	} else {
		pharmacy.UpdatePharmacy(data.Id, data.Name)
		c.JSON(http.StatusOK, util.RS{Message: "更新成功", Status: true})
	}
}
