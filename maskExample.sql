/*
 Navicat Premium Data Transfer

 Source Server         : tttxxx52
 Source Server Type    : MariaDB
 Source Server Version : 100147
 Source Host           : 139.162.106.67:33069
 Source Schema         : kdan_test

 Target Server Type    : MariaDB
 Target Server Version : 100147
 File Encoding         : 65001

 Date: 12/03/2021 17:37:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for masks
-- ----------------------------
DROP TABLE IF EXISTS `masks`;
CREATE TABLE `masks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pharmacy_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(32,16) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1正常。2.刪除 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of masks
-- ----------------------------
BEGIN;
INSERT INTO `masks` VALUES (1, 1, 'AniMask (blue) (10 per pack)', 33.6500000000000000, 1);
INSERT INTO `masks` VALUES (2, 2, 'MaskT (black) (10 per pack)', 14.9000000000000000, 1);
INSERT INTO `masks` VALUES (3, 2, 'Free to Roam (black) (3 per pack)', 13.8300000000000000, 1);
INSERT INTO `masks` VALUES (4, 2, 'AniMask (green) (10 per pack)', 49.2100000000000000, 1);
INSERT INTO `masks` VALUES (5, 2, 'Masquerade (blue) (6 per pack)', 16.7500000000000000, 1);
INSERT INTO `masks` VALUES (6, 3, 'Second Smile (blue) (10 per pack)', 39.9800000000000000, 1);
INSERT INTO `masks` VALUES (7, 3, 'Free to Roam (green) (3 per pack)', 8.8300000000000000, 1);
INSERT INTO `masks` VALUES (8, 3, 'AniMask (black) (3 per pack)', 12.8100000000000000, 1);
INSERT INTO `masks` VALUES (9, 3, 'Masquerade (black) (3 per pack)', 8.1700000000000000, 1);
INSERT INTO `masks` VALUES (10, 3, 'MaskT (blue) (3 per pack)', 7.0400000000000000, 1);
INSERT INTO `masks` VALUES (11, 4, 'Masquerade (black) (10 per pack)', 19.5400000000000000, 1);
INSERT INTO `masks` VALUES (12, 4, 'Free to Roam (blue) (10 per pack)', 30.7400000000000000, 1);
INSERT INTO `masks` VALUES (13, 4, 'Free to Roam (black) (10 per pack)', 26.5400000000000000, 1);
INSERT INTO `masks` VALUES (14, 4, 'AniMask (blue) (6 per pack)', 28.7200000000000000, 1);
INSERT INTO `masks` VALUES (15, 4, 'MaskT (black) (3 per pack)', 4.1400000000000000, 1);
INSERT INTO `masks` VALUES (16, 4, 'Second Smile (green) (3 per pack)', 6.5500000000000000, 1);
INSERT INTO `masks` VALUES (17, 5, 'Masquerade (black) (3 per pack)', 3.7600000000000000, 1);
INSERT INTO `masks` VALUES (18, 5, 'Free to Roam (blue) (3 per pack)', 7.8900000000000000, 1);
INSERT INTO `masks` VALUES (19, 5, 'MaskT (green) (10 per pack)', 32.5700000000000000, 1);
INSERT INTO `masks` VALUES (20, 5, 'AniMask (green) (10 per pack)', 22.0100000000000000, 1);
INSERT INTO `masks` VALUES (21, 5, 'Masquerade (green) (10 per pack)', 42.2700000000000000, 1);
INSERT INTO `masks` VALUES (22, 5, 'AniMask (black) (6 per pack)', 14.1600000000000000, 1);
INSERT INTO `masks` VALUES (23, 6, 'MaskT (green) (6 per pack)', 29.9100000000000000, 1);
INSERT INTO `masks` VALUES (24, 6, 'Second Smile (green) (6 per pack)', 11.8900000000000000, 1);
INSERT INTO `masks` VALUES (25, 6, 'MaskT (green) (10 per pack)', 35.0600000000000000, 1);
INSERT INTO `masks` VALUES (26, 6, 'Free to Roam (black) (3 per pack)', 5.3100000000000000, 1);
INSERT INTO `masks` VALUES (27, 7, 'AniMask (green) (10 per pack)', 10.8300000000000000, 1);
INSERT INTO `masks` VALUES (28, 7, 'AniMask (black) (3 per pack)', 8.9400000000000000, 1);
INSERT INTO `masks` VALUES (29, 7, 'Masquerade (blue) (6 per pack)', 20.0000000000000000, 1);
INSERT INTO `masks` VALUES (30, 7, 'Masquerade (blue) (10 per pack)', 21.6700000000000000, 1);
INSERT INTO `masks` VALUES (31, 7, 'Second Smile (blue) (3 per pack)', 7.3200000000000000, 1);
INSERT INTO `masks` VALUES (32, 8, 'MaskT (black) (6 per pack)', 13.4100000000000000, 1);
INSERT INTO `masks` VALUES (33, 9, 'Masquerade (black) (3 per pack)', 10.2800000000000000, 1);
INSERT INTO `masks` VALUES (34, 9, 'Free to Roam (green) (3 per pack)', 14.1800000000000000, 1);
INSERT INTO `masks` VALUES (35, 9, 'MaskT (green) (10 per pack)', 47.8300000000000000, 1);
INSERT INTO `masks` VALUES (36, 10, 'Free to Roam (blue) (10 per pack)', 38.4100000000000000, 1);
INSERT INTO `masks` VALUES (37, 10, 'MaskT (blue) (6 per pack)', 27.9100000000000000, 1);
INSERT INTO `masks` VALUES (38, 10, 'Free to Roam (black) (6 per pack)', 28.5400000000000000, 1);
INSERT INTO `masks` VALUES (39, 10, 'Masquerade (blue) (6 per pack)', 28.0200000000000000, 1);
INSERT INTO `masks` VALUES (40, 11, 'Second Smile (black) (3 per pack)', 5.0600000000000000, 1);
INSERT INTO `masks` VALUES (41, 11, 'Second Smile (blue) (3 per pack)', 12.5100000000000000, 1);
INSERT INTO `masks` VALUES (42, 11, 'Free to Roam (black) (3 per pack)', 10.8100000000000000, 1);
INSERT INTO `masks` VALUES (43, 11, 'Second Smile (black) (6 per pack)', 23.7300000000000000, 1);
INSERT INTO `masks` VALUES (44, 11, 'MaskT (black) (10 per pack)', 43.9400000000000000, 1);
INSERT INTO `masks` VALUES (45, 11, 'MaskT (black) (3 per pack)', 10.1300000000000000, 1);
INSERT INTO `masks` VALUES (46, 12, 'Free to Roam (blue) (3 per pack)', 5.6100000000000000, 1);
INSERT INTO `masks` VALUES (47, 12, 'MaskT (black) (10 per pack)', 46.5100000000000000, 1);
INSERT INTO `masks` VALUES (48, 12, 'Second Smile (black) (3 per pack)', 9.5900000000000000, 1);
INSERT INTO `masks` VALUES (49, 12, 'Masquerade (blue) (3 per pack)', 6.2600000000000000, 1);
INSERT INTO `masks` VALUES (50, 12, 'AniMask (blue) (10 per pack)', 11.4700000000000000, 1);
INSERT INTO `masks` VALUES (51, 13, 'Second Smile (green) (6 per pack)', 6.8200000000000000, 1);
INSERT INTO `masks` VALUES (52, 13, 'MaskT (green) (6 per pack)', 25.7300000000000000, 1);
INSERT INTO `masks` VALUES (53, 13, 'MaskT (black) (6 per pack)', 9.7000000000000000, 1);
INSERT INTO `masks` VALUES (54, 14, 'AniMask (green) (10 per pack)', 25.4200000000000000, 1);
INSERT INTO `masks` VALUES (55, 14, 'AniMask (blue) (10 per pack)', 34.3900000000000000, 1);
INSERT INTO `masks` VALUES (56, 14, 'Second Smile (black) (3 per pack)', 12.3800000000000000, 1);
INSERT INTO `masks` VALUES (57, 14, 'MaskT (green) (3 per pack)', 11.5600000000000000, 1);
INSERT INTO `masks` VALUES (58, 14, 'AniMask (blue) (6 per pack)', 19.1600000000000000, 1);
INSERT INTO `masks` VALUES (59, 14, 'AniMask (black) (10 per pack)', 36.3100000000000000, 1);
INSERT INTO `masks` VALUES (60, 14, 'Free to Roam (black) (6 per pack)', 20.1600000000000000, 1);
INSERT INTO `masks` VALUES (61, 14, 'AniMask (green) (3 per pack)', 3.2300000000000000, 1);
INSERT INTO `masks` VALUES (62, 14, 'Masquerade (green) (3 per pack)', 5.7300000000000000, 1);
INSERT INTO `masks` VALUES (63, 15, 'Masquerade (blue) (10 per pack)', 38.3300000000000000, 1);
INSERT INTO `masks` VALUES (64, 16, 'MaskT (black) (6 per pack)', 12.9000000000000000, 1);
INSERT INTO `masks` VALUES (65, 16, 'AniMask (black) (3 per pack)', 10.6500000000000000, 1);
INSERT INTO `masks` VALUES (66, 16, 'MaskT (black) (3 per pack)', 4.8300000000000000, 1);
INSERT INTO `masks` VALUES (67, 16, 'Free to Roam (green) (3 per pack)', 13.9300000000000000, 1);
INSERT INTO `masks` VALUES (68, 17, 'Masquerade (black) (3 per pack)', 7.3300000000000000, 1);
INSERT INTO `masks` VALUES (69, 17, 'Masquerade (green) (6 per pack)', 26.6000000000000000, 1);
INSERT INTO `masks` VALUES (70, 17, 'AniMask (black) (6 per pack)', 6.5300000000000000, 1);
INSERT INTO `masks` VALUES (71, 18, 'Masquerade (blue) (6 per pack)', 13.5500000000000000, 1);
INSERT INTO `masks` VALUES (72, 18, 'MaskT (black) (10 per pack)', 46.6900000000000000, 1);
INSERT INTO `masks` VALUES (73, 18, 'Free to Roam (blue) (10 per pack)', 15.7900000000000000, 1);
INSERT INTO `masks` VALUES (74, 18, 'Second Smile (green) (6 per pack)', 17.6100000000000000, 1);
INSERT INTO `masks` VALUES (75, 18, 'Free to Roam (black) (10 per pack)', 35.6600000000000000, 1);
INSERT INTO `masks` VALUES (76, 18, 'MaskT (blue) (6 per pack)', 28.2700000000000000, 1);
INSERT INTO `masks` VALUES (77, 18, 'MaskT (green) (10 per pack)', 39.4000000000000000, 1);
INSERT INTO `masks` VALUES (78, 18, 'Free to Roam (blue) (3 per pack)', 14.6100000000000000, 1);
INSERT INTO `masks` VALUES (79, 18, 'Second Smile (black) (3 per pack)', 13.5200000000000000, 1);
INSERT INTO `masks` VALUES (80, 19, 'Masquerade (green) (6 per pack)', 12.7000000000000000, 1);
INSERT INTO `masks` VALUES (81, 20, 'Second Smile (green) (6 per pack)', 14.9000000000000000, 1);
INSERT INTO `masks` VALUES (82, 20, 'AniMask (blue) (3 per pack)', 9.2400000000000000, 1);
COMMIT;

-- ----------------------------
-- Table structure for pharmacy
-- ----------------------------
DROP TABLE IF EXISTS `pharmacy`;
CREATE TABLE `pharmacy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `balance` decimal(32,16) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常 2刪除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pharmacy
-- ----------------------------
BEGIN;
INSERT INTO `pharmacy` VALUES (1, 'Better You', 777.6100000000000000, 1);
INSERT INTO `pharmacy` VALUES (2, 'Cash Saver Pharmacy', 596.9400000000000000, 1);
INSERT INTO `pharmacy` VALUES (3, 'PrecisionMed', 181.8400000000000000, 1);
INSERT INTO `pharmacy` VALUES (4, 'MedSavvy', 903.5700000000000000, 1);
INSERT INTO `pharmacy` VALUES (5, 'Pill Pack', 905.4400000000000000, 1);
INSERT INTO `pharmacy` VALUES (6, 'Heartland Pharmacy', 858.9100000000000000, 1);
INSERT INTO `pharmacy` VALUES (7, 'Longhorn Pharmacy', 323.3000000000000000, 1);
INSERT INTO `pharmacy` VALUES (8, 'PharmaMed', 238.8900000000000000, 1);
INSERT INTO `pharmacy` VALUES (9, 'Neighbors', 151.6500000000000000, 1);
INSERT INTO `pharmacy` VALUES (10, 'Discount Drugs', 753.1800000000000000, 1);
INSERT INTO `pharmacy` VALUES (11, 'Medlife', 467.3900000000000000, 1);
INSERT INTO `pharmacy` VALUES (12, 'Pride Pharmacy', 896.7500000000000000, 1);
INSERT INTO `pharmacy` VALUES (13, 'Atlas Drugs', 785.0200000000000000, 1);
INSERT INTO `pharmacy` VALUES (14, 'Thrifty Way Pharmacy', 220.7300000000000000, 1);
INSERT INTO `pharmacy` VALUES (15, 'Apotheco', 274.4900000000000000, 1);
INSERT INTO `pharmacy` VALUES (16, 'Drug Blend', 767.1400000000000000, 1);
INSERT INTO `pharmacy` VALUES (17, 'Wellcare', 898.0600000000000000, 1);
INSERT INTO `pharmacy` VALUES (18, 'Assured Rx', 181.7600000000000000, 1);
INSERT INTO `pharmacy` VALUES (19, 'RxToMe', 510.9100000000000000, 1);
INSERT INTO `pharmacy` VALUES (20, 'DFW Wellness', 466.3600000000000000, 1);
COMMIT;

-- ----------------------------
-- Table structure for pharmacy_business_hours
-- ----------------------------
DROP TABLE IF EXISTS `pharmacy_business_hours`;
CREATE TABLE `pharmacy_business_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pharmacy_id` int(11) NOT NULL DEFAULT '0',
  `days` int(1) NOT NULL DEFAULT '0' COMMENT '星期幾  1:星期一  ~  7:星期日',
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `end_time` time NOT NULL DEFAULT '00:00:00',
  `is_off_day` int(1) NOT NULL DEFAULT '1' COMMENT '1:正常. 2:休息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pharmacy_business_hours
-- ----------------------------
BEGIN;
INSERT INTO `pharmacy_business_hours` VALUES (1, 1, 1, '12:56:00', '21:58:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (2, 1, 2, '13:06:00', '22:42:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (3, 1, 3, '12:56:00', '21:58:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (4, 1, 4, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (5, 1, 5, '17:09:00', '20:20:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (6, 1, 6, '17:09:00', '20:20:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (7, 1, 7, '07:10:00', '09:33:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (8, 2, 1, '11:00:00', '14:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (9, 2, 2, '00:05:00', '07:20:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (10, 2, 3, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (11, 2, 4, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (12, 2, 5, '00:05:00', '07:20:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (13, 2, 6, '09:01:00', '12:43:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (14, 2, 7, '09:01:00', '12:43:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (15, 3, 1, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (16, 3, 2, '14:10:00', '16:25:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (17, 3, 3, '16:57:00', '21:46:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (18, 3, 4, '16:30:00', '19:40:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (19, 3, 5, '02:55:00', '16:49:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (20, 3, 6, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (21, 3, 7, '10:59:00', '05:33:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (22, 4, 1, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (23, 4, 2, '10:08:00', '23:13:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (24, 4, 3, '12:38:00', '21:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (25, 4, 4, '12:14:00', '22:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (26, 4, 5, '15:01:00', '21:24:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (27, 4, 6, '15:01:00', '21:24:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (28, 4, 7, '00:03:00', '07:58:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (29, 5, 1, '07:14:00', '17:06:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (30, 5, 2, '16:47:00', '19:25:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (31, 5, 3, '15:30:00', '19:00:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (32, 5, 4, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (33, 5, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (34, 5, 6, '04:35:00', '06:35:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (35, 5, 7, '01:39:00', '16:59:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (36, 6, 1, '13:18:00', '17:49:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (37, 6, 2, '05:06:00', '17:45:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (38, 6, 3, '03:25:00', '11:25:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (39, 6, 4, '03:25:00', '11:25:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (40, 6, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (41, 6, 6, '04:10:00', '08:03:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (42, 6, 7, '15:07:00', '18:50:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (43, 7, 1, '10:53:00', '16:49:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (44, 7, 2, '17:41:00', '21:42:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (45, 7, 3, '10:53:00', '16:49:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (46, 7, 4, '08:25:00', '00:30:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (47, 7, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (48, 7, 6, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (49, 7, 7, '15:53:00', '02:05:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (50, 8, 1, '03:27:00', '09:16:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (51, 8, 2, '14:41:00', '19:40:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (52, 8, 3, '04:05:00', '16:06:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (53, 8, 4, '09:49:00', '17:25:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (54, 8, 5, '08:05:00', '19:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (55, 8, 6, '03:27:00', '09:16:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (56, 8, 7, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (57, 9, 1, '10:09:00', '02:26:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (58, 9, 2, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (59, 9, 3, '15:26:00', '17:33:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (60, 9, 4, '15:31:00', '17:46:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (61, 9, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (62, 9, 6, '13:14:00', '20:24:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (63, 9, 7, '00:02:00', '16:40:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (64, 10, 1, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (65, 10, 2, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (66, 10, 3, '05:16:00', '09:37:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (67, 10, 4, '14:04:00', '23:19:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (68, 10, 5, '00:27:00', '04:08:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (69, 10, 6, '00:27:00', '04:08:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (70, 10, 7, '03:04:00', '06:25:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (71, 11, 1, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (72, 11, 2, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (73, 11, 3, '16:49:00', '20:32:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (74, 11, 4, '15:57:00', '09:13:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (75, 11, 5, '13:36:00', '20:51:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (76, 11, 6, '13:36:00', '20:51:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (77, 11, 7, '02:42:00', '19:44:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (78, 12, 1, '07:50:00', '14:53:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (79, 12, 2, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (80, 12, 3, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (81, 12, 4, '00:53:00', '07:57:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (82, 12, 5, '00:53:00', '07:57:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (83, 12, 6, '12:20:00', '17:45:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (84, 12, 7, '15:50:00', '10:49:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (85, 13, 1, '16:30:00', '23:07:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (86, 13, 2, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (87, 13, 3, '10:16:00', '16:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (88, 13, 4, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (89, 13, 5, '16:20:00', '20:39:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (90, 13, 6, '00:17:00', '12:55:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (91, 13, 7, '05:33:00', '23:59:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (92, 14, 1, '04:02:00', '15:08:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (93, 14, 2, '09:57:00', '18:23:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (94, 14, 3, '12:10:00', '00:10:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (95, 14, 4, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (96, 14, 5, '04:02:00', '15:08:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (97, 14, 6, '12:21:00', '21:32:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (98, 14, 7, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (99, 15, 1, '10:06:00', '14:26:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (100, 15, 2, '08:27:00', '22:13:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (101, 15, 3, '08:06:00', '16:22:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (102, 15, 4, '12:24:00', '19:49:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (103, 15, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (104, 15, 6, '10:06:00', '14:26:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (105, 15, 7, '15:53:00', '05:32:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (106, 16, 1, '04:08:00', '20:52:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (107, 16, 2, '01:01:00', '06:01:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (108, 16, 3, '11:18:00', '20:37:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (109, 16, 4, '16:44:00', '23:43:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (110, 16, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (111, 16, 6, '11:18:00', '20:37:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (112, 16, 7, '04:26:00', '14:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (113, 17, 1, '07:59:00', '14:55:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (114, 17, 2, '08:57:00', '04:59:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (115, 17, 3, '17:12:00', '22:13:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (116, 17, 4, '01:57:00', '06:48:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (117, 17, 5, '08:57:00', '04:59:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (118, 17, 6, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (119, 17, 7, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (120, 18, 1, '02:30:00', '06:43:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (121, 18, 2, '08:44:00', '11:28:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (122, 18, 3, '08:44:00', '11:28:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (123, 18, 4, '02:06:00', '05:27:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (124, 18, 5, '05:24:00', '16:59:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (125, 18, 6, '02:30:00', '06:43:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (126, 18, 7, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (127, 19, 1, '07:12:00', '11:46:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (128, 19, 2, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (129, 19, 3, '16:24:00', '20:15:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (130, 19, 4, '08:59:00', '14:07:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (131, 19, 5, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (132, 19, 6, '10:47:00', '12:50:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (133, 19, 7, '10:47:00', '12:50:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (134, 20, 1, '00:20:00', '16:06:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (135, 20, 2, '00:20:00', '16:06:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (136, 20, 3, '10:02:00', '13:23:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (137, 20, 4, '10:02:00', '09:41:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (138, 20, 5, '16:08:00', '21:01:00', 1);
INSERT INTO `pharmacy_business_hours` VALUES (139, 20, 6, '00:00:00', '00:00:00', 2);
INSERT INTO `pharmacy_business_hours` VALUES (140, 20, 7, '10:02:00', '13:23:00', 1);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `balance` decimal(32,16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Eric Underwood', 952.6900000000000000);
INSERT INTO `users` VALUES (2, 'Peggy Maxwell', 508.1900000000000000);
INSERT INTO `users` VALUES (3, 'Sherri Lynch', 759.1500000000000000);
INSERT INTO `users` VALUES (4, 'Mindy Perkins', 857.4800000000000000);
INSERT INTO `users` VALUES (5, 'Ruby Andrews', 791.8700000000000000);
INSERT INTO `users` VALUES (6, 'Jo Barton', 690.9900000000000000);
INSERT INTO `users` VALUES (7, 'Jose May', 504.1000000000000000);
INSERT INTO `users` VALUES (8, 'Ricky Anderson', 266.0600000000000000);
INSERT INTO `users` VALUES (9, 'Frances Collier', 584.5200000000000000);
INSERT INTO `users` VALUES (10, 'Eula Wheeler', 110.1800000000000000);
INSERT INTO `users` VALUES (11, 'Mae Hill', 291.8800000000000000);
INSERT INTO `users` VALUES (12, 'Tamara Dean', 488.4700000000000000);
INSERT INTO `users` VALUES (13, 'Lawrence Fletcher', 149.9100000000000000);
INSERT INTO `users` VALUES (14, 'Cassandra Fields', 933.1800000000000000);
INSERT INTO `users` VALUES (15, 'Audrey Brewer', 509.5500000000000000);
INSERT INTO `users` VALUES (16, 'Juan Estrada', 530.5000000000000000);
INSERT INTO `users` VALUES (17, 'Viola Quinn', 153.5600000000000000);
INSERT INTO `users` VALUES (18, 'Ada Larson', 579.9800000000000000);
INSERT INTO `users` VALUES (19, 'Connie Vasquez', 545.5000000000000000);
INSERT INTO `users` VALUES (20, 'Bobbie Russell', 161.9100000000000000);
COMMIT;

-- ----------------------------
-- Table structure for users_orders
-- ----------------------------
DROP TABLE IF EXISTS `users_orders`;
CREATE TABLE `users_orders` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `masks_id` int(11) NOT NULL,
  `price` decimal(32,16) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_orders
-- ----------------------------
BEGIN;
INSERT INTO `users_orders` VALUES (1, 1, 33, 9.2600000000000000, 1, '2021-01-02 20:41:02');
INSERT INTO `users_orders` VALUES (2, 1, 80, 12.0100000000000000, 1, '2021-01-04 11:29:05');
INSERT INTO `users_orders` VALUES (3, 1, 53, 8.9000000000000000, 1, '2021-01-07 03:49:25');
INSERT INTO `users_orders` VALUES (4, 1, 51, 7.3000000000000000, 1, '2021-01-22 09:27:11');
INSERT INTO `users_orders` VALUES (5, 1, 10, 7.0100000000000000, 1, '2021-01-26 03:41:14');
INSERT INTO `users_orders` VALUES (6, 2, 63, 38.0300000000000000, 1, '2021-01-08 13:36:55');
INSERT INTO `users_orders` VALUES (7, 2, 10, 6.8800000000000000, 1, '2021-01-09 02:23:32');
INSERT INTO `users_orders` VALUES (8, 2, 8, 13.8600000000000000, 1, '2021-01-11 00:00:08');
INSERT INTO `users_orders` VALUES (9, 2, 59, 39.6300000000000000, 1, '2021-01-20 18:31:28');
INSERT INTO `users_orders` VALUES (10, 2, 52, 27.4100000000000000, 1, '2021-01-28 11:56:40');
INSERT INTO `users_orders` VALUES (11, 2, 40, 4.6000000000000000, 1, '2021-01-30 07:01:12');
INSERT INTO `users_orders` VALUES (12, 3, 36, 38.7200000000000000, 1, '2021-01-07 22:59:35');
INSERT INTO `users_orders` VALUES (13, 3, 69, 26.4900000000000000, 1, '2021-01-09 06:34:26');
INSERT INTO `users_orders` VALUES (14, 3, 82, 8.7100000000000000, 1, '2021-01-17 13:13:56');
INSERT INTO `users_orders` VALUES (15, 3, 2, 14.3400000000000000, 1, '2021-01-18 03:33:09');
INSERT INTO `users_orders` VALUES (16, 4, 80, 12.5200000000000000, 1, '2021-01-02 05:00:08');
INSERT INTO `users_orders` VALUES (17, 4, 80, 11.9200000000000000, 1, '2021-01-03 11:50:47');
INSERT INTO `users_orders` VALUES (18, 4, 63, 35.2700000000000000, 1, '2021-01-11 17:54:43');
INSERT INTO `users_orders` VALUES (19, 4, 40, 5.2000000000000000, 1, '2021-01-12 21:00:59');
INSERT INTO `users_orders` VALUES (20, 4, 63, 41.9200000000000000, 1, '2021-01-14 16:22:48');
INSERT INTO `users_orders` VALUES (21, 4, 80, 12.0200000000000000, 1, '2021-01-16 20:39:46');
INSERT INTO `users_orders` VALUES (22, 4, 52, 27.4500000000000000, 1, '2021-01-20 19:19:28');
INSERT INTO `users_orders` VALUES (23, 5, 36, 41.3500000000000000, 1, '2021-01-05 21:10:10');
INSERT INTO `users_orders` VALUES (24, 5, 7, 8.3900000000000000, 1, '2021-01-18 09:44:02');
INSERT INTO `users_orders` VALUES (25, 5, 28, 8.9700000000000000, 1, '2021-01-19 12:34:04');
INSERT INTO `users_orders` VALUES (26, 5, 1, 31.4700000000000000, 1, '2021-01-21 22:42:24');
INSERT INTO `users_orders` VALUES (27, 5, 63, 34.6500000000000000, 1, '2021-01-25 02:46:28');
INSERT INTO `users_orders` VALUES (28, 5, 80, 13.6700000000000000, 1, '2021-01-25 09:01:09');
INSERT INTO `users_orders` VALUES (29, 5, 37, 29.1000000000000000, 1, '2021-01-28 07:47:49');
INSERT INTO `users_orders` VALUES (30, 5, 27, 11.5000000000000000, 1, '2021-01-29 07:50:51');
INSERT INTO `users_orders` VALUES (31, 6, 80, 13.0400000000000000, 1, '2021-01-02 19:09:19');
INSERT INTO `users_orders` VALUES (32, 6, 44, 47.1000000000000000, 1, '2021-01-04 04:43:21');
INSERT INTO `users_orders` VALUES (33, 6, 32, 13.6800000000000000, 1, '2021-01-09 16:28:11');
INSERT INTO `users_orders` VALUES (34, 6, 81, 16.0800000000000000, 1, '2021-01-12 14:08:04');
INSERT INTO `users_orders` VALUES (35, 6, 44, 46.5000000000000000, 1, '2021-01-13 02:19:26');
INSERT INTO `users_orders` VALUES (36, 6, 52, 27.2200000000000000, 1, '2021-01-24 06:42:24');
INSERT INTO `users_orders` VALUES (37, 7, 74, 17.3000000000000000, 1, '2021-01-13 08:26:03');
INSERT INTO `users_orders` VALUES (38, 8, 63, 39.2900000000000000, 1, '2021-01-10 18:21:02');
INSERT INTO `users_orders` VALUES (39, 8, 1, 34.1100000000000000, 1, '2021-01-14 20:36:52');
INSERT INTO `users_orders` VALUES (40, 8, 40, 4.7700000000000000, 1, '2021-01-22 05:53:35');
INSERT INTO `users_orders` VALUES (41, 8, 36, 41.8300000000000000, 1, '2021-01-24 19:38:39');
INSERT INTO `users_orders` VALUES (42, 8, 24, 12.4200000000000000, 1, '2021-01-29 06:09:31');
INSERT INTO `users_orders` VALUES (43, 9, 82, 10.0200000000000000, 1, '2021-01-19 20:53:57');
INSERT INTO `users_orders` VALUES (44, 9, 48, 9.0600000000000000, 1, '2021-01-30 17:50:39');
INSERT INTO `users_orders` VALUES (45, 10, 64, 14.0100000000000000, 1, '2021-01-11 04:29:31');
INSERT INTO `users_orders` VALUES (46, 11, 63, 35.7300000000000000, 1, '2021-01-09 03:09:01');
INSERT INTO `users_orders` VALUES (47, 11, 24, 10.8200000000000000, 1, '2021-01-12 17:12:59');
INSERT INTO `users_orders` VALUES (48, 11, 81, 16.2600000000000000, 1, '2021-01-13 16:06:08');
INSERT INTO `users_orders` VALUES (49, 11, 10, 7.2800000000000000, 1, '2021-01-14 13:13:17');
INSERT INTO `users_orders` VALUES (50, 11, 4, 48.5200000000000000, 1, '2021-01-18 00:31:18');
INSERT INTO `users_orders` VALUES (51, 11, 5, 17.0000000000000000, 1, '2021-01-19 00:20:59');
INSERT INTO `users_orders` VALUES (52, 11, 12, 27.8400000000000000, 1, '2021-01-24 00:07:03');
INSERT INTO `users_orders` VALUES (53, 11, 6, 39.5300000000000000, 1, '2021-01-25 07:12:20');
INSERT INTO `users_orders` VALUES (54, 11, 45, 10.8300000000000000, 1, '2021-01-26 22:47:36');
INSERT INTO `users_orders` VALUES (55, 12, 14, 28.5200000000000000, 1, '2021-01-02 12:07:07');
INSERT INTO `users_orders` VALUES (56, 12, 1, 32.3600000000000000, 1, '2021-01-05 00:08:49');
INSERT INTO `users_orders` VALUES (57, 12, 80, 13.1600000000000000, 1, '2021-01-07 10:00:00');
INSERT INTO `users_orders` VALUES (58, 12, 39, 26.5500000000000000, 1, '2021-01-11 18:31:44');
INSERT INTO `users_orders` VALUES (59, 12, 53, 9.5300000000000000, 1, '2021-01-12 18:50:36');
INSERT INTO `users_orders` VALUES (60, 12, 20, 22.0500000000000000, 1, '2021-01-16 15:46:54');
INSERT INTO `users_orders` VALUES (61, 12, 64, 11.6300000000000000, 1, '2021-01-22 02:13:55');
INSERT INTO `users_orders` VALUES (62, 12, 14, 27.6700000000000000, 1, '2021-01-23 23:38:40');
INSERT INTO `users_orders` VALUES (63, 12, 70, 6.3000000000000000, 1, '2021-01-25 16:06:27');
INSERT INTO `users_orders` VALUES (64, 13, 58, 18.9600000000000000, 1, '2021-01-01 15:57:40');
INSERT INTO `users_orders` VALUES (65, 13, 2, 15.7900000000000000, 1, '2021-01-07 12:20:58');
INSERT INTO `users_orders` VALUES (66, 13, 4, 46.6400000000000000, 1, '2021-01-08 07:34:26');
INSERT INTO `users_orders` VALUES (67, 13, 14, 27.5700000000000000, 1, '2021-01-14 11:41:41');
INSERT INTO `users_orders` VALUES (68, 13, 4, 44.4500000000000000, 1, '2021-01-22 19:01:35');
INSERT INTO `users_orders` VALUES (69, 13, 80, 11.8300000000000000, 1, '2021-01-24 04:40:43');
INSERT INTO `users_orders` VALUES (70, 13, 33, 9.5800000000000000, 1, '2021-01-27 19:47:28');
INSERT INTO `users_orders` VALUES (71, 14, 8, 12.4900000000000000, 1, '2021-01-03 17:36:46');
INSERT INTO `users_orders` VALUES (72, 14, 80, 12.1100000000000000, 1, '2021-01-21 04:10:05');
INSERT INTO `users_orders` VALUES (73, 14, 71, 13.0500000000000000, 1, '2021-01-26 02:28:08');
INSERT INTO `users_orders` VALUES (74, 14, 26, 5.3000000000000000, 1, '2021-01-27 02:29:55');
INSERT INTO `users_orders` VALUES (75, 15, 76, 27.9300000000000000, 1, '2021-01-18 16:37:03');
INSERT INTO `users_orders` VALUES (76, 16, 8, 13.9300000000000000, 1, '2021-01-08 06:08:22');
INSERT INTO `users_orders` VALUES (77, 17, 65, 10.4900000000000000, 1, '2021-01-05 18:53:24');
INSERT INTO `users_orders` VALUES (78, 17, 46, 5.1300000000000000, 1, '2021-01-17 23:14:20');
INSERT INTO `users_orders` VALUES (79, 18, 46, 5.4200000000000000, 1, '2021-01-01 20:13:53');
INSERT INTO `users_orders` VALUES (80, 18, 1, 32.2200000000000000, 1, '2021-01-10 03:21:55');
INSERT INTO `users_orders` VALUES (81, 18, 38, 26.3900000000000000, 1, '2021-01-13 18:56:13');
INSERT INTO `users_orders` VALUES (82, 18, 2, 14.1100000000000000, 1, '2021-01-15 15:10:28');
INSERT INTO `users_orders` VALUES (83, 18, 69, 26.0800000000000000, 1, '2021-01-17 14:48:26');
INSERT INTO `users_orders` VALUES (84, 18, 23, 32.6400000000000000, 1, '2021-01-19 16:44:30');
INSERT INTO `users_orders` VALUES (85, 18, 54, 24.7400000000000000, 1, '2021-01-25 08:00:44');
INSERT INTO `users_orders` VALUES (86, 19, 20, 19.9600000000000000, 1, '2021-01-06 08:01:31');
INSERT INTO `users_orders` VALUES (87, 19, 80, 13.5800000000000000, 1, '2021-01-08 21:56:50');
INSERT INTO `users_orders` VALUES (88, 19, 21, 38.0900000000000000, 1, '2021-01-09 18:04:01');
INSERT INTO `users_orders` VALUES (89, 19, 53, 10.1100000000000000, 1, '2021-01-10 07:42:34');
INSERT INTO `users_orders` VALUES (90, 19, 82, 9.4500000000000000, 1, '2021-01-22 15:51:01');
INSERT INTO `users_orders` VALUES (91, 19, 37, 28.6800000000000000, 1, '2021-01-25 01:06:21');
INSERT INTO `users_orders` VALUES (92, 19, 69, 27.6800000000000000, 1, '2021-01-27 16:46:47');
INSERT INTO `users_orders` VALUES (93, 19, 2, 14.3600000000000000, 1, '2021-01-28 03:01:56');
INSERT INTO `users_orders` VALUES (94, 20, 10, 7.2600000000000000, 1, '2021-01-01 06:05:32');
INSERT INTO `users_orders` VALUES (95, 20, 36, 35.0300000000000000, 1, '2021-01-01 22:54:04');
INSERT INTO `users_orders` VALUES (96, 20, 33, 10.0800000000000000, 1, '2021-01-18 08:25:29');
INSERT INTO `users_orders` VALUES (97, 20, 41, 11.8000000000000000, 1, '2021-01-25 05:32:22');
INSERT INTO `users_orders` VALUES (98, 20, 82, 9.6000000000000000, 1, '2021-01-27 14:02:37');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
